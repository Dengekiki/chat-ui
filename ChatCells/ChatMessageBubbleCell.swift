//
//  ChatMessageBubbleCell.swift
//  ChatCells
//
//  Created by Taylor Miller-Klugman on 12/30/18.
//  Copyright © 2018 Taylor Miller-Klugman. All rights reserved.
//

import UIKit


class ChatMessageBubbleCell: UITableViewCell {
    
    
    var trailingConstraint: NSLayoutConstraint!
    var leadingConstraint: NSLayoutConstraint!
    
    var chatMessage: chatmssg! {
        didSet {
            bubbleBgView.backgroundColor = chatMessage.isIncomming ? #colorLiteral(red: 0.3411764801, green: 0.6235294342, blue: 0.1686274558, alpha: 1) : #colorLiteral(red: 0.4392156899, green: 0.01176470611, blue: 0.1921568662, alpha: 1)
            mssgLabel.textColor = chatMessage.isIncomming ? #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1) : #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            mssgLabel.text = chatMessage.txt
            
            if chatMessage.isIncomming {
                leadingConstraint.isActive = true
                trailingConstraint.isActive = false
            } else {
                leadingConstraint.isActive = false
                trailingConstraint.isActive = true
            }
        }
    }
    
    
    let mssgLabel = UILabel()
    let bubbleBgView = UIView()
    
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        backgroundColor = .clear
        bubbleBgView.translatesAutoresizingMaskIntoConstraints = false
        bubbleBgView.layer.cornerRadius = 12
        
        addSubview(bubbleBgView)
        addSubview(mssgLabel)
        
        //mssglabel style
        mssgLabel.translatesAutoresizingMaskIntoConstraints = false
        mssgLabel.numberOfLines = 0
        
        //layouts
        let constraints = [ mssgLabel.topAnchor.constraint(equalTo: topAnchor, constant: 32),
        mssgLabel.bottomAnchor.constraint(equalTo: bottomAnchor,constant: -32),
        mssgLabel.widthAnchor.constraint(lessThanOrEqualToConstant: 250),
        
        bubbleBgView.topAnchor.constraint(equalTo: mssgLabel.topAnchor, constant: -16),
        bubbleBgView.leadingAnchor.constraint(equalTo: mssgLabel.leadingAnchor, constant: -16),
        bubbleBgView.bottomAnchor.constraint(equalTo: mssgLabel.bottomAnchor, constant: 16),
        bubbleBgView.trailingAnchor.constraint(equalTo: mssgLabel.trailingAnchor, constant: 16),

        
        ]
        
        NSLayoutConstraint.activate(constraints)
        
         leadingConstraint = mssgLabel.leadingAnchor.constraint(equalTo: leadingAnchor,constant: 32)
         trailingConstraint = mssgLabel.trailingAnchor.constraint(equalTo: trailingAnchor,constant: -32)
        
        leadingConstraint.isActive = false
        trailingConstraint.isActive = true
        
        
    }
    
   

    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
